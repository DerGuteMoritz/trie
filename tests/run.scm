(cond-expand
  (chicken-4 (use test))
  (chicken-5 (import (chicken load) test)))

(load-relative "../trie.scm")
(import trie)

(define trie
  (make-trie))

(test-assert (not (trie-ref trie (string->list "hey"))))

(trie-insert! trie (string->list "hey") 'foo)

(test 'foo (trie-ref trie (string->list "hey")))

(trie-insert! trie (string->list "heyo") 'bar)
(trie-insert! trie (string->list "hex") 'baz)

(test 'foo (trie-ref trie (string->list "hey")))
(test 'bar (trie-ref trie (string->list "heyo")))
(test 'baz (trie-ref trie (string->list "hex")))

(test-exit)
